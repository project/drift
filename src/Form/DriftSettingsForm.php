<?php

namespace Drupal\drift\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DriftSettingsForm
 *
 * @package Drupal\drift\Form
 */
class DriftSettingsForm extends ConfigFormBase {
  /**
   * The typed config service.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager
  ) {
    parent::__construct($config_factory);
    $this->typedConfigManager = $typedConfigManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drift_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'drift.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drift.settings');
    $drift_config_schema = $this->typedConfigManager->getDefinition('drift.settings') + ['mapping' => []];
    $drift_config_schema = $drift_config_schema['mapping'];

    $form['status'] = [
      '#type' => 'select',
      '#title' => $drift_config_schema['status']['label'],
      '#description' => $drift_config_schema['status']['description'],
      '#default_value' => $config->get('status'),
      '#options' => [ 1 => 'Enabled', 0 => 'Disabled']
    ];

    $form['identifier'] = [
      '#type' => 'textfield',
      '#title' => $drift_config_schema['identifier']['label'],
      '#description' => $drift_config_schema['identifier']['description'],
      '#default_value' => $config->get('identifier'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration
    $this->configFactory->getEditable('drift.settings')
      // Set the submitted configuration setting
      ->set('status', $form_state->getValue('status'))
      // You can set multiple configurations at once by making
      // multiple calls to set()
      ->set('identifier', $form_state->getValue('identifier'))
      ->save();

    //Flushing JS cache to avoid caching of old identifier.
    \Drupal::service('asset.js.collection_optimizer')
      ->deleteAll();

    _drupal_flush_css_js();

    parent::submitForm($form, $form_state);
  }

}
